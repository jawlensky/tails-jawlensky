# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:26+0100\n"
"PO-Revision-Date: 2023-03-11 11:29+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Creating and using LUKS encrypted volumes\"]]\n"
msgstr "[[!meta title=\"Создание и использование зашифрованных томов LUKS\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Introduction to <span class=\"application\">LUKS</span>"
msgid "<h1 id=\"luks\">Introduction to <span class=\"application\">LUKS</span></h1>\n"
msgstr "Общие сведения о <span class=\"application\">LUKS</span>"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<p>The simplest way to carry around the documents that you want to use\n"
#| "with Tails encrypted is to use the encrypted [[Persistent\n"
#| "Storage|first_steps/persistence]].</p>\n"
msgid ""
"<p>The simplest way to carry around the documents that you want to use\n"
"with Tails encrypted is to use the [[Persistent\n"
"Storage|persistent_storage]].</p>\n"
msgstr ""
"<p>Самый простой способ сохранять зашифрованные документы в Tails — использовать\n"
" зашифрованное [[Постоянное хранилище|first_steps/persistence]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid ""
"You can create other encrypted volumes using\n"
"<span class=\"application\">LUKS</span> to encrypt, for example, another\n"
"USB stick or an external hard disk.\n"
"<span class=\"application\">LUKS</span> is the standard for disk\n"
"encryption in Linux.\n"
msgstr ""
"Для создания зашифрованных томов (например, флешек, внешних жёстких дисков) можно использовать <span class=\"application\">LUKS</span>. Это стандарт шифрования дисков\n"
"в Linux.\n"

#. type: Plain text
#, fuzzy
#| msgid "The GNOME desktop allows you to open encrypted volumes."
msgid "- The *Disks* utility allows you to create encrypted volumes."
msgstr "Их можно открыть на рабочем столе GNOME."

#. type: Plain text
#, fuzzy
#| msgid "The GNOME desktop allows you to open encrypted volumes."
msgid "- The GNOME desktop allows you to open encrypted volumes."
msgstr "Их можно открыть на рабочем столе GNOME."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Comparison between <span class=\"application\">LUKS</span> and <span class=\"application\">VeraCrypt</span>"
msgid "<h2 id=\"comparison\">Comparison between <span class=\"application\">LUKS</span> and <span class=\"application\">VeraCrypt</span></h2>\n"
msgstr "Сравнение <span class=\"application\">LUKS</span> и <span class=\"application\">VeraCrypt</span>"

#. type: Plain text
#, no-wrap
msgid ""
"You can also open <span class=\"application\">VeraCrypt</span> encrypted\n"
"volumes in Tails. <span class=\"application\">VeraCrypt</span> is a disk\n"
"encryption tool for Windows, macOS, and Linux. [[See our documentation\n"
"about <span class=\"application\">VeraCrypt</span>.|veracrypt]]\n"
msgstr "Вы можете открывать в Tails тома, зашифрованные с помощью <span class=\"application\">VeraCrypt</span>. Это шифровальная программа, которая работает в Windows, macOS и Linux. [[См. наше описание <span class=\"application\">VeraCrypt</span>.|veracrypt]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/encryption_and_privacy/luks_vs_veracrypt.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/encryption_and_privacy/luks_vs_veracrypt.inline.ru\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<h2 class=\"bullet-number-three\">Create a new encrypted partition</h2>\n"
msgid "<h1 id=\"creating\">Creating an encrypted partition</h1>\n"
msgstr "<h2 class=\"bullet-number-three\">Erstellen Sie eine neue, verschlüsselte Partition</h2>\n"

#. type: Bullet: '1. '
msgid ""
"Choose **Applications**&nbsp;▸ **Utilities**&nbsp;▸ **Disks** to open the "
"*Disks* utility."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "<h2 id=\"identifying\">Identifying your external storage device</h2>\n"
msgstr "<h2 class=\"bullet-number-one\">Identifizieren Sie das externe Speichermedium</h2>\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "<span class=\"application\">Disks</span> lists all the current storage\n"
#| "devices on the left side of the screen.\n"
msgid ""
"The *Disks* utility lists all the current storage devices on the left side "
"of the screen."
msgstr ""
"В левой части окна утилиты <span class=\"application\">Диски</span> видны "
"все подключённые носители данных.\n"

#. type: Bullet: '  1. '
msgid "Plug in the external storage device that you want to use."
msgstr "Подключите внешний носитель данных."

#. type: Bullet: '  1. '
#, fuzzy
#| msgid "A new device appears in the list of storage devices. Click on it:"
msgid "A new device appears in the list of storage devices. Click on it."
msgstr "В списке слева появится новое устройство. Щёлкните по нему:"

#. type: Plain text
#, no-wrap
msgid "     [[!img storage_devices_after.png link=no alt=\"\"]]\n"
msgstr "     [[!img storage_devices_after.png link=no alt=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  1. Check that the description of the device on the right side of the screen\n"
"  corresponds to your device: its brand, its size, etc.\n"
msgstr "  1. Убедитесь, что описание в правой части окна соответствует вашему устройству: модель, объём и т.д.\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<h2 class=\"bullet-number-two\">Format the device</h2>\n"
msgid "<h2 id=\"formatting\">Formatting the device</h2>\n"
msgstr "<h2 class=\"bullet-number-two\">Formatieren Sie das Medium</h2>\n"

#. type: Bullet: '  1. '
#, fuzzy
#| msgid ""
#| "Click on the <span class=\"guimenu\">[[!img lib/open-menu.png "
#| "alt=\"Menu\" class=\"symbolic\" link=\"no\"]]</span> button in the "
#| "titlebar and choose <span class=\"guilabel\">Format Disk…</span> to erase "
#| "all the existing partitions on the device."
msgid ""
"Click on the [[!img lib/view-more.png alt=\"Drive Options\" "
"class=\"symbolic\" link=\"no\"]] button in the title bar and choose **Format "
"Disk** to erase all the existing partitions on the device."
msgstr ""
"Нажмите кнопку <span class=\"guimenu\">[[!img lib/open-menu.png alt=\"Меню\" "
"class=\"symbolic\" link=\"no\"]]</span> в заголовке окна. Выберите <span "
"class=\"guilabel\">Форматировать диск...</span>, чтобы удалить все "
"существующие разделы на устройстве."

#. type: Bullet: '  1. '
#, fuzzy
#| msgid "In the <span class=\"guilabel\">Format Disk</span> dialog:"
msgid "In the **Format Disk** dialog:"
msgstr "Появится диалог <span class=\"guilabel\">Форматировать диск</span>."

#. type: Bullet: '     - '
#, fuzzy
#| msgid ""
#| "If you want to securely erase all data on the device, choose to <span "
#| "class=\"guilabel\">Overwrite existing data with zeroes</span> in the "
#| "<span class=\"guilabel\">Erase</span> drop-down list."
msgid ""
"If you want to overwrite all data on the device, choose **Overwrite existing "
"data with zeroes** in the **Erase** menu."
msgstr ""
"Если хотите надёжно удалить все данные с устройства, в раскрывающемся списке "
"<span class=\"guilabel\">Очистить</span> выберите <span "
"class=\"guilabel\">Перезаписывать существующие данные нулями</span>."

#. type: Plain text
#, no-wrap
msgid ""
"       <div class=\"caution\">\n"
"       <p>Overwriting existing data does not erase all data on flash\n"
"       memories, such as USB sticks and SSDs (Solid-State Drives).</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "       <p>See the [[limitations of file deletion|doc/encryption_and_privacy/secure_deletion#spare]].</p>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "</div>\n"
msgid "       </div>\n"
msgstr "</div>\n"

#. type: Bullet: '     - '
#, fuzzy
#| msgid ""
#| "Choose <span class=\"guilabel\">Compatible with all systems and devices "
#| "(MBR/DOS)</span> in the <span class=\"guilabel\">Partitioning</span> drop-"
#| "down list."
msgid ""
"Choose **Compatible with all systems and devices (MBR/DOS)** in the "
"**Partitioning** menu."
msgstr ""
"Выберите в раскрывающемся списке <span class=\"guilabel\">Разметка</span> "
"вариант <span class=\"guilabel\">Совместимо со всеми системами и "
"устройствами (MBR / DOS)</span>."

#. type: Bullet: '  1. '
msgid "Click **Format**."
msgstr ""

#. type: Bullet: '  1. '
#, fuzzy
#| msgid ""
#| "In the confirmation dialog, make sure that the device is correct. Click "
#| "<span class=\"bold\">Format</span> to confirm."
msgid "In the confirmation dialog, make sure that the device is correct."
msgstr ""
"В диалоговом окне убедитесь, что устройство выбрано верно. Нажмите <span "
"class=\"bold\">Форматировать</span> для подтверждения."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<h2 class=\"bullet-number-three\">Create a new encrypted partition</h2>\n"
msgid "<h2 id=\"partition\">Creating a new encrypted partition</h2>\n"
msgstr "<h2 class=\"bullet-number-three\">Erstellen Sie eine neue, verschlüsselte Partition</h2>\n"

#. type: Plain text
msgid ""
"Now the schema of the partitions in the middle of the screen shows an empty "
"device:"
msgstr "Теперь схема разделов в середине экрана показывает пустое устройство:"

#. type: Plain text
#, no-wrap
msgid "[[!img empty_device.png link=no alt=\"Free Space 123 GB\"]]\n"
msgstr "[[!img empty_device.png link=no alt=\"Свободное место 123 Гб\"]]\n"

#. type: Bullet: '  1. '
msgid ""
"Click on the <span class=\"guimenu\">[[!img lib/list-add.png alt=\"Create "
"partition\" class=\"symbolic\" link=\"no\"]]</span> button to create a new "
"partition on the device."
msgstr ""
"Нажмите кнопку <span class=\"guimenu\">[[!img lib/list-add.png alt=\"Создать "
"раздел\" class=\"symbolic\" link=\"no\"]]</span>, чтобы создать новый раздел "
"на устройстве."

#. type: Plain text
#, no-wrap
msgid ""
"  1. Configure the various settings of your new partition in the\n"
"  partition creation assistant:\n"
msgstr "  1. Настройте новый раздел в помощнике по созданию разделов:\n"

#. type: Bullet: '     - '
msgid "In the <span class=\"guilabel\">Create Partition</span> screen:"
msgstr "В окне <span class=\"guilabel\">Создать раздел</span>:"

#. type: Bullet: '       - '
msgid ""
"<span class=\"guilabel\">Partition Size</span>: you can create a partition "
"on the whole device or only on part of it."
msgstr ""
"<span class=\"guilabel\">Размер раздела</span>. Можно создать раздел, "
"занимающий всё устройство или только его часть."

#. type: Plain text
#, no-wrap
msgid "         In the example below, we are creating a partition of 4.0 GB on a device of 8.1 GB.\n"
msgstr "         В этом примере мы создадим раздел  4,0 Гб на устройстве 8,1 Гб.\n"

#. type: Bullet: '     - '
#, fuzzy
#| msgid "In the <span class=\"guilabel\">Format Disk</span> dialog:"
msgid "In the **Format Volume** dialog:"
msgstr "Появится диалог <span class=\"guilabel\">Форматировать диск</span>."

#. type: Bullet: '       - '
msgid "**Volume Name**"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<span class=\"guilabel\">Volume Name</span>: you can give a name to the partition.  This name remains invisible until the partition is open but can help you to identify it during use."
msgid ""
"         You can give a name to the partition.\n"
"         This name remains invisible until the partition is open but can help\n"
"         you to identify it during use.\n"
msgstr "<span class=\"guilabel\">Имя тома</span>. Вы можете дать название разделу. Оно остаётся невидимым до тех пор, пока раздел не будет открыт, и может помочь вам идентифицировать его во время работы."

#. type: Bullet: '       - '
msgid "**Erase**"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "         You can choose to overwrite existing data on the partition.\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<div class=\"tip\">\n"
msgid "         <div class=\"caution\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"         <p>Overwriting existing data does not erase all data on flash\n"
"         memories, such as USB sticks and SSDs (Solid-State Drives).</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "         <p>See the [[limitations of file deletion|doc/encryption_and_privacy/secure_deletion#spare]].</p>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "</div>\n"
msgid "         </div>\n"
msgstr "</div>\n"

#. type: Bullet: '       - '
msgid "**Type**"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<span class=\"guilabel\">Type</span>: choose <span class=\"guilabel\">Internal disk for use with Linux systems only (Ext4)</span> and <span class=\"guilabel\">Password protect volume (LUKS)</span>."
msgid ""
"         Choose **Internal disk for use with Linux systems\n"
"         only (Ext4)** and **Password protect volume (LUKS)**.\n"
msgstr "<span class=\"guilabel\">Тип</span>. Выберите <span class=\"guilabel\">Внутренний диск для использования только с системами Linux (Ext4)</span> и <span class=\"guilabel\">Защищённый паролем том (LUKS)</span>."

#. type: Bullet: '     - '
msgid "In the <span class=\"guilabel\">Set Password</span> screen:"
msgstr "В окне <span class=\"guilabel\">Установить пароль</span>:"

#. type: Bullet: '       - '
msgid ""
"<span class=\"guilabel\">Password</span>: type a passphrase for the "
"encrypted partition and repeat it to confirm."
msgstr ""
"<span class=\"guilabel\">Пароль.</span> Введите пароль для зашифрованного "
"тома и повторите его для подтверждения."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<div class=\"tip\">\n"
msgid "         <div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"         <p>We recommend choosing a long passphrase made of 5 to 7 random words.\n"
"         <a href=\"https://theintercept.com/2015/03/26/passphrases-can-memorize-attackers-cant-guess/\">Learn\n"
"         about the maths behind memorizable and secure passphrases.</a></p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"         <div class=\"caution\">\n"
"         <p>It is impossible to recover your passphrase if you forget it!</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"         <p>To help you remember your passphrase, you can write it on a piece of\n"
"         paper, store it in your wallet for a few days, and destroy it once\n"
"         you know it well.</p>\n"
"         </div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "       Then click <span class=\"bold\">Create</span>.\n"
msgstr "       Затем нажмите <span class=\"bold\">Создать</span>.\n"

#. type: Bullet: '  1. '
msgid ""
"Creating the partition takes from a few seconds to a few minutes. After "
"that, the new encrypted partition appears in the volumes on the device:"
msgstr ""
"Создание раздела занимает от нескольких секунд до нескольких минут. Примерно "
"так выглядит результат:"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "     [[!img encrypted_partition.png link=\"no\" alt=\"Partition 1 4.0 GB LUKS / secret 4.0 GB Ext4\"]]\n"
msgid "     [[!img encrypted_partition.png link=\"no\" alt=\"Partition 1 4.0 GB LUKS / Filesystem 4.0 GB Ext4\"]]\n"
msgstr "     [[!img encrypted_partition.png link=\"no\" alt=\"Раздел 1 4.0 Гб LUKS / зашифрован 4.0 Гб Ext4\"]]\n"

#. type: Bullet: '  1. '
msgid ""
"If you want to create another partition in the free space on the device, "
"click on the free space and then click on the <span class=\"guimenu\">[[!img "
"lib/list-add.png alt=\"Create partition\" class=\"symbolic\" link=\"no\"]]</"
"span> button again."
msgstr ""
"Если хотите создать ещё один раздел в свободном пространстве на устройстве, "
"щёлкните по свободному месту, а затем снова нажмите кнопку <span "
"class=\"guimenu\">[[!img lib/list-add.png alt=\"Создать раздел\" "
"class=\"symbolic\" link=\"no\"]]</span>."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<h2 class=\"bullet-number-four\">Use the new partition</h2>\n"
msgid "<h2 id=\"using\">Using the new partition</h2>\n"
msgstr "<h2 class=\"bullet-number-four\">Benutzen Sie die neue Partition</h2>\n"

#. type: Plain text
msgid ""
"You can open this new partition from the sidebar of the file browser with "
"the name you gave it."
msgstr ""
"Новый раздел можно открыть в боковой панели файлового менеджера с той меткой "
"(именем), которую вы ему присвоили."

#. type: Plain text
#, no-wrap
msgid ""
"After opening the partition with the file browser, you can also access it\n"
"from the <span class=\"guimenu\">Places</span> menu.\n"
msgstr ""
"После открытия раздела с помощью файлового менеджера вы также можете получить к нему доступ\n"
"из меню <span class=\"guimenu\">Места</span>.\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Open an existing encrypted partition\n"
msgid "<h1 id=\"opening\">Opening an existing encrypted partition</h1>\n"
msgstr "Öffnen Sie eine bestehende, verschlüsselte Partition\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "When plugging in a device containing an encrypted partition, Tails does "
#| "not open the partition automatically but you can do so from the file "
#| "browser."
msgid ""
"When you plug in a device that has an encrypted partition, Tails offers to "
"unlock the encryption automatically."
msgstr ""
"При подключении устройства с зашифрованным разделом Tails не открывает этот "
"раздел автоматически. Вы можете сделать это с помощью файлового менеджера."

#. type: Bullet: '1. '
#, fuzzy
#| msgid "Plug in the external storage device that you want to use."
msgid "Plug in the external storage device that has the encrypted partition."
msgstr "Подключите внешний носитель данных."

#. type: Bullet: '1. '
msgid "A dialog appears, asking for the passphrase to unlock the partition."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img unlock.png link=\"no\" class=\"screenshot\" alt=\"Authentication Required\"]]\n"
msgstr ""

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "Enter the passphrase of the partition in the password prompt and click "
#| "<span class=\"bold\">Unlock</span>."
msgid ""
"Enter the passphrase of the partition in the password prompt and click "
"**Unlock**."
msgstr ""
"Введите пароль раздела в поле для ввода пароля и нажмите <span "
"class=\"bold\">Разблокировать</span>."

#. type: Plain text
#, no-wrap
msgid ""
"   If you turn on the option **Remember Password**, Tails remembers the\n"
"   passphrase of this partition only until you shut down. The passphrase is not\n"
"   stored in your Persistent Storage.\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"After unlocking, you can access the content of the partition from either:"
msgstr ""

#. type: Bullet: '   * '
msgid "The **Places** menu"
msgstr ""

#. type: Bullet: '   * '
msgid "The sidebar of the *Files* browser"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "To close the partition after you finished using it, click on the <span class=\"guimenu\">[[!img lib/media-eject.png alt=\"Eject\" class=\"symbolic\" link=\"no\"]]</span> button next to the partition in the sidebar of the file browser."
msgid ""
"After you finished using the partition, click on the [[!img lib/media-eject.png\n"
"alt=\"Eject\" class=\"symbolic\" link=\"no\"]] button next to the partition in the\n"
"sidebar of the *Files* browser to eject the partition safety and lock again the\n"
"encryption.\n"
msgstr "Чтобы закрыть раздел после окончания работы нажмите кнопку <span class=\"guimenu\">[[!img lib/media-eject.png alt=\"Извлечь\" class=\"symbolic\" link=\"no\"]]</span> рядом с разделом на боковой панели файлового менеджера."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Storing sensitive documents\n"
msgid "<h1 id=\"storing\">Storing sensitive documents</h1>\n"
msgstr "Sensible Dokumente speichern\n"

#. type: Plain text
msgid ""
"Such encrypted volumes are not hidden. An attacker in possession of the "
"device can know that there is an encrypted volume on it. Take into "
"consideration that you can be forced or tricked to give out its passphrase."
msgstr ""
"Описанные нами зашифрованные тома не скрыты от чужих глаз. Если "
"злоумышленник получит физический доступ к устройству, он может выяснить, что "
"на устройстве есть зашифрованный том. В определённых ситуациях вас могут "
"заставить сообщить пароль или обманным путем завладеть им."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Opening encrypted volumes from other operating systems\n"
msgid "<h1 id=\"os\">Opening encrypted volumes from other operating systems</h1>\n"
msgstr "Öffnen verschlüsselter Laufwerke auf anderen Betriebssystemen\n"

#. type: Plain text
msgid ""
"It is possible to open such encrypted volumes from other operating systems. "
"But, doing so might compromise the security provided by Tails."
msgstr ""
"Зашифрованные тома можно открывать и в других операционных системах. Но это "
"может поставить под сомнение защиту, обеспечиваемую Tails."

#. type: Plain text
msgid ""
"For example, image thumbnails might be created and saved by the other "
"operating system. Or, the contents of files might be indexed by the other "
"operating system."
msgstr ""
"Например, в другой операционной системе могут быть созданы (и сохранены) "
"значки изображений. Содержимое файлов тоже может быть проиндексировано."

#. type: Plain text
#, fuzzy, no-wrap
msgid "<h1 id=\"changing\">Changing the passphrase of an existing encrypted partition</h1>\n"
msgstr "Öffnen Sie eine bestehende, verschlüsselte Partition\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "1. Plug in the external storage device containing the encrypted partition "
#| "that you\n"
#| "want to change the passphrase for.\n"
msgid ""
"Plug in the external storage device that contains the encrypted partition "
"for which you want to change the passphrase."
msgstr ""
"1. Подключите внешний носитель с зашифрованным разделом, для которого вы\n"
"хотите изменить пароль.\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid "The device appears in the list of storage devices. Click on it:"
msgid "The device appears in the list of storage devices. Click on it."
msgstr "Устройство появится в списке носителей. Щёлкните по нему:"

#. type: Plain text
#, no-wrap
msgid "   [[!img storage_devices_after.png link=no alt=\"\"]]\n"
msgstr "   [[!img storage_devices_after.png link=no alt=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"1. Check that the description of the device on the right side of the screen\n"
"corresponds to your device: its brand, its size, etc.\n"
msgstr ""
"1. Убедитесь, что описание в правой части экрана\n"
"соответствует вашему устройству: модель, объём и т.д.\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "Click on the partition displaying a [[!img lib/network-wireless-encrypted."
#| "png alt=\"padlock\" class=\"symbolic\" link=\"no\"]] at the bottom-right "
#| "corner."
msgid ""
"Click on the partition displaying a [[!img lib/network-wireless-encrypted."
"png alt=\"Parition LUKS\" class=\"symbolic\" link=\"no\"]] in the bottom-"
"right corner."
msgstr ""
"Нажмите на раздел со значком [[!img lib/network-wireless-encrypted.png "
"alt=\"замочек\" class=\"symbolic\" link=\"no\"]] в правом нижнем углу."

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "   [[!img lib/system-run.png alt=\"Additional partition options\" "
#| "class=\"symbolic\" link=\"no\"]]\n"
msgid ""
"Click on the [[!img lib/system-run.png alt=\"Additional partition options\" "
"class=\"symbolic\" link=\"no\"]] button and choose **Change Passphrase** in "
"the shortcut menu."
msgstr ""
"   кнопке [[!img lib/system-run.png alt=\"Дополнительные параметры раздела\" "
"class=\"symbolic\" link=\"no\"]]\n"

#~ msgid "In the <span class=\"guilabel\">Format Volume</span> screen:"
#~ msgstr "В окне <span class=\"guilabel\">Форматировать том</span>:"

#~ msgid ""
#~ "<span class=\"guilabel\">Erase</span>: you can choose to securely erase "
#~ "all data on the partition."
#~ msgstr ""
#~ "<span class=\"guilabel\">Очистить</span>. Можно удалить все данные "
#~ "раздела надёжным способом."

#, fuzzy, no-wrap
#~| msgid ""
#~| "         <div class=\"caution\">\n"
#~| "         <p>Secure deletion does not work as expected on USB sticks and\n"
#~| "         SSDs (Solid-State Drives). Choose instead to overwrite existing\n"
#~| "         data on the whole device when [[formatting the\n"
#~| "         device|encrypted_volumes#format]].</p>\n"
#~| "         <p>See also our [[warning about secure deletion on USB sticks and\n"
#~| "         SSDs|secure_deletion#usb_and_ssd]].</p>\n"
#~| "         </div>\n"
#~ msgid ""
#~ "         <div class=\"caution\">\n"
#~ "         <p>Secure deletion does not work as expected on USB sticks and\n"
#~ "         SSDs (Solid-State Drives). Choose instead to overwrite existing\n"
#~ "         data on the whole device when [[formatting the\n"
#~ "         device|encrypted_volumes#formatting]].</p>\n"
#~ "         <p>See also our [[warning about secure deletion on USB sticks and\n"
#~ "         SSDs|secure_deletion#usb_and_ssd]].</p>\n"
#~ "         </div>\n"
#~ msgstr ""
#~ "         <класс div=\"caution\">\n"
#~ "<p>Безопасное удаление не работает должным образом на флешках и\n"
#~ "SSD (твердотельных накопителях). Вместо этого при [[форматировании\n"
#~ "устройства|encrypted_volumes#format]] выбирайте перезапись существующих\n"
#~ "данных на всём устройстве.</p>\n"
#~ "<p>См. также наши [[рекомендации о безопасном удалении данных на флешках и\n"
#~ "твердотельных накопителях|secure_deletion#usb_and_ssd]].</p>\n"
#~ "</div>\n"

#~ msgid ""
#~ "<span class=\"application\">GNOME Disks</span> allows you to create "
#~ "encrypted volumes."
#~ msgstr ""
#~ "Утилита <span class=\"application\">Диски</span> в GNOME позволяет "
#~ "создавать зашифрованные тома."

#, fuzzy, no-wrap
#~| msgid "     Then click <span class=\"bold\">Format…</span>.\n"
#~ msgid "     Then click <span class=\"bold\">Format…</span>.\n"
#~ msgstr "     Затем нажмите <span class=\"bold\">Форматировать…</span>.\n"

#, fuzzy, no-wrap
#~| msgid ""
#~| "1. Choose\n"
#~| "   <span class=\"menuchoice\">\n"
#~| "     <span class=\"guimenu\">Places</span>&nbsp;▸\n"
#~| "     <span class=\"guisubmenu\">Computer</span></span>\n"
#~| "   to open the file browser.\n"
#~ msgid ""
#~ "1. Choose\n"
#~ "   <span class=\"menuchoice\">\n"
#~ "     <span class=\"guimenu\">Places</span>&nbsp;▸\n"
#~ "     <span class=\"guisubmenu\">Computer</span></span>\n"
#~ "   to open the file browser.\n"
#~ msgstr ""
#~ "1. Выберите\n"
#~ "   <span class=\"menuchoice\">\n"
#~ "     <span class=\"guimenu\">Места</span>&nbsp;▸\n"
#~ "     <span class=\"guisubmenu\">Компьютер</span></span>\n"
#~ "   для того, чтобы открыть файловый менеджер.\n"

#~ msgid ""
#~ "Click on the encrypted partition that you want to open in the sidebar."
#~ msgstr ""
#~ "В левой панели нажмите на зашифрованный раздел, который хотите открыть."

#, no-wrap
#~ msgid "   [[!img nautilus_encrypted.png link=\"no\" alt=\"File browser with '4.0 GB Encrypted' entry in the sidebar\"]]\n"
#~ msgstr "   [[!img nautilus_encrypted.png link=\"no\" alt=\"Файловый менеджер с зашифрованным томом\"]]\n"

#~ msgid ""
#~ "After opening the partition with the file browser, you can also access it "
#~ "from the <span class=\"guimenu\">Places</span> menu."
#~ msgstr ""
#~ "После открытия раздела с помощью файлового менеджера вы также можете "
#~ "получить к нему доступ из меню <span class=\"guimenu\">Места</span> ."

#, fuzzy, no-wrap
#~| msgid ""
#~| "To open <span class=\"application\">GNOME Disks</span> choose\n"
#~| "<span class=\"menuchoice\">\n"
#~| "  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#~| "  <span class=\"guisubmenu\">Utilities</span>&nbsp;▸\n"
#~| "  <span class=\"guimenuitem\">Disks</span></span>.\n"
#~ msgid ""
#~ "To open <span class=\"application\">GNOME Disks</span> choose\n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#~ "  <span class=\"guisubmenu\">Utilities</span>&nbsp;▸\n"
#~ "  <span class=\"guimenuitem\">Disks</span></span>.\n"
#~ msgstr ""
#~ "Чтобы открыть утилиту <span class=\"application\">Диски</span>, используйте\n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Приложения</span>&nbsp;▸\n"
#~ "  <span class=\"guisubmenu\">Утилиты</span>&nbsp;▸\n"
#~ "  <span class=\"guimenuitem\">Диски</span></span>.\n"

#~ msgid "Click on the <span class=\"guimenu\">"
#~ msgstr "Щёлкните по <span class=\"guimenu\">"

#, fuzzy, no-wrap
#~| msgid "   </span> button and choose <span class=\"guimenu\">Change Passphrase…</span>\n"
#~ msgid "   </span> button and choose <span class=\"guimenu\">Change Passphrase…</span>\n"
#~ msgstr "   </span> и выберите <span class=\"guimenu\">Изменить пароль…</span>\n"

#, no-wrap
#~ msgid "Create an encrypted partition"
#~ msgstr "Создание зашифрованного раздела"

#, no-wrap
#~ msgid "Identify your external storage device"
#~ msgstr "Выбор внешнего носителя данных"

#, no-wrap
#~ msgid "<a id=\"format\"></a>\n"
#~ msgstr "<a id=\"format\"></a>\n"

#, no-wrap
#~ msgid "Format the device"
#~ msgstr "Форматирование носителя"

#, no-wrap
#~ msgid "Create a new encrypted partition"
#~ msgstr "Создание нового зашифрованного раздела"

#, no-wrap
#~ msgid "Use the new partition"
#~ msgstr "Использование нового раздела"

#, no-wrap
#~ msgid "Open an existing encrypted partition"
#~ msgstr "Открытие существующего зашифрованного раздела"

#, no-wrap
#~ msgid "Storing sensitive documents"
#~ msgstr "Хранение важных документов"

#, no-wrap
#~ msgid "Opening encrypted volumes from other operating systems"
#~ msgstr "Открытие зашифрованных томов в других операционных системах"

#, no-wrap
#~ msgid "<a id=\"change\"></a>\n"
#~ msgstr "<a id=\"change\"></a>\n"

#, no-wrap
#~ msgid "Change the passphrase of an existing encrypted partition"
#~ msgstr "Смена пароля зашифрованного раздела"

#, fuzzy
#~ msgid "[[!meta title=\"Create and use LUKS encrypted volumes\"]]\n"
#~ msgstr ""
#~ "[[!meta title=\"Erstellen und Benutzen verschlüsselter Laufwerke\"]]\n"

#~ msgid "Create an encrypted partition\n"
#~ msgstr "Eine verschlüsselte Partition erstellen\n"

#, fuzzy
#~ msgid "Create a new encrypted partition\n"
#~ msgstr "Eine verschlüsselte Partition erstellen\n"

#, fuzzy
#~ msgid "Use the new partition\n"
#~ msgstr "Konfigurieren Sie die neue Partition:"

#~ msgid ""
#~ "The simplest way to carry around the documents you want to use with Tails "
#~ "and make sure that they haven't been accessed or modified is to store "
#~ "them in an encrypted volume: a dedicated partition on a USB stick or "
#~ "external hard-disk."
#~ msgstr ""
#~ "Der einfachste Weg, um Dokumente, die Sie mit Tails verwenden möchten, "
#~ "mit sich zu tragen und dabei sicherzustellen, dass auf diese nicht "
#~ "zugegriffen wurde oder diese nicht modifiziert wurden, ist, sie in einem "
#~ "verschlüsselten Laufwerk zu speichern: einer dedizierten Partition auf "
#~ "einem USB-Stick oder einer externen Festplatte."

#~ msgid ""
#~ "Tails comes with utilities for LUKS, a standard for disk-encryption under "
#~ "Linux."
#~ msgstr ""
#~ "Tails kommt zusammen mit Werkzeugen für LUKS, einem Standard für "
#~ "Laufwerksverschlüsselung unter Linux."

#, fuzzy
#~ msgid ""
#~ "<p>To store encrypted files on a Tails USB stick, it is recommended to "
#~ "create a\n"
#~ "[[persistent volume|first_steps/persistence]] instead.</p>\n"
#~ msgstr ""
#~ "<p>Um verschlüsselte Dateien auf einem Tails Medium zu sichern, wird "
#~ "empfohlen, stattdessen einen\n"
#~ "[[beständigen Speicherbereich|first_steps/persistence]] zu erstellen.</"
#~ "p>\n"

#~ msgid ""
#~ "  1. Click on <span class=\"guilabel\">Format Drive</span> to erase all "
#~ "the\n"
#~ "  existing partitions on the device.\n"
#~ msgstr ""
#~ "  1. Wählen Sie <span class=\"guilabel\">Datenträger formatieren</span> "
#~ "aus, um alle\n"
#~ "  auf dem Medium existierenden Partitionen zu löschen.\n"

#~ msgid ""
#~ "  1. In the dialog box to select the <span class=\"guilabel\">Scheme</"
#~ "span>, if\n"
#~ "  you are unsure, leave the default option <span "
#~ "class=\"guilabel\">Master Boot\n"
#~ "  Record</span> selected.\n"
#~ msgstr ""
#~ "  1. Falls Sie sich unsicher sind, lassen Sie in dem Dialogfenster zum "
#~ "Auswählen des <span\n"
#~ "   class=\"guilabel\">Schema</span> den voreingestellten Wert <span "
#~ "class=\"guilabel\">Master Boot\n"
#~ "  Record</span> ausgewählt.\n"

#~ msgid ""
#~ "     [[!img create_partition.png link=no alt=\"Create partition on…\"]]\n"
#~ msgstr ""
#~ "     [[!img create_partition.png link=no alt=\"Create partition on…\"]]\n"

#~ msgid ""
#~ "<span class=\"guilabel\">Type</span>. You can change the file system type "
#~ "of the partition. If you are not sure you can leave the default value: "
#~ "<span class=\"guilabel\">Ext4</span>."
#~ msgstr ""
#~ "<span class=\"guilabel\">Typ</span>. Sie können das Dateisystem der "
#~ "Partition ändern. Wenn Sie sich nicht sicher sind, können Sie den "
#~ "voreingestellten Wert belassen: <span class=\"guilabel\">Ext4</span>."

#~ msgid "[[!img places_secret.png link=no alt=\"Places&nbsp;▸ secret\"]]\n"
#~ msgstr "[[!img places_secret.png link=no alt=\"Places&nbsp;▸ secret\"]]\n"

#~ msgid ""
#~ "When plugging in a device containing an encrypted partition, Tails does "
#~ "not mount it\n"
#~ "automatically but it appears in the <span class=\"guimenu\">Places</"
#~ "span>\n"
#~ "menu. If several partitions appear as <span class=\"guimenu\">Encrypted</"
#~ "span>,\n"
#~ "like in the example, you can use their sizes to guess which one is the "
#~ "one you want\n"
#~ "to open.\n"
#~ msgstr ""
#~ "Wenn ein Medium, welches eine verschlüsselte Partition enthält, "
#~ "angeschlossen wird, hängt Tails\n"
#~ "es nicht automatisch ein, es erscheint aber im <span "
#~ "class=\"guimenu\">Orte</span>-Menü.\n"
#~ "Wenn mehrere Partitionen, wie in dem Beispiel, als <span "
#~ "class=\"guimenu\">Verschlüsselt</span> angezeigt\n"
#~ "werden, können Sie ihre Größe dazu benutzen, um zu erraten, welche jene "
#~ "ist, die Sie öffnen\n"
#~ "möchten.\n"
